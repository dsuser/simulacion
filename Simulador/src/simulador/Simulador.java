package simulador;

import java.awt.Color;
import java.awt.Image;
import java.awt.Toolkit;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYSplineRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

public final class Simulador extends javax.swing.JFrame {

    Sistema sistema;
    MessageConsole consola;

    public Simulador() {
        initComponents();
        Image icon = Toolkit.getDefaultToolkit().getImage(getClass().getResource("imagenes/icono.png"));
        setIconImage(icon);
        consola = new MessageConsole(Consola);
        consola.redirectOut(Color.BLACK, System.out);
        habilitarP(false);
    }

    public void habilitarP(boolean v) {
        DesdePl.setEnabled(v);
        DesdePw.setEnabled(v);
        DesdePx.setEnabled(v);
        HastaPl.setEnabled(v);
        HastaPw.setEnabled(v);
        HastaPx.setEnabled(v);
        CalcularPl.setEnabled(v);
        CalcularPw.setEnabled(v);
        CalcularPx.setEnabled(v);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Help = new javax.swing.JFrame();
        ScrollHelp = new javax.swing.JScrollPane();
        TextHelp = new javax.swing.JTextArea();
        Modelo = new javax.swing.JPanel();
        L_lamda = new javax.swing.JLabel();
        L_miu = new javax.swing.JLabel();
        user1 = new javax.swing.JLabel();
        user2 = new javax.swing.JLabel();
        vlamda = new javax.swing.JTextField();
        vmiu = new javax.swing.JTextField();
        Analizar = new javax.swing.JButton();
        L_Tiempo = new javax.swing.JLabel();
        vtiempo = new javax.swing.JTextField();
        user3 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        vpercentil = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        vlpercentil = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        confianza = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        Lsistema = new javax.swing.JLabel();
        Resultados = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        labelPercentil = new javax.swing.JLabel();
        percentil = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        mediaAtencion = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        desviacionAtencion = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        mediaLlegada = new javax.swing.JTextField();
        desviacionLlegada = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        lpercentil = new javax.swing.JTextField();
        jPanel5 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        intervaloInicio = new javax.swing.JTextField();
        intervaloFin = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        L_Congestion = new javax.swing.JLabel();
        L_LongitudSistema = new javax.swing.JLabel();
        L_LongitudCola = new javax.swing.JLabel();
        Rho = new javax.swing.JTextField();
        L = new javax.swing.JTextField();
        Lq = new javax.swing.JTextField();
        jPanel7 = new javax.swing.JPanel();
        L_TiempoSistema = new javax.swing.JLabel();
        L_TiempoCola = new javax.swing.JLabel();
        Wq = new javax.swing.JTextField();
        W = new javax.swing.JTextField();
        jPanel8 = new javax.swing.JPanel();
        L_Eficiencia = new javax.swing.JLabel();
        L_X = new javax.swing.JLabel();
        Ex = new javax.swing.JTextField();
        Eficiencia = new javax.swing.JTextField();
        PanelConsola = new javax.swing.JPanel();
        ScrollConsola = new javax.swing.JScrollPane();
        Consola = new javax.swing.JTextPane();
        Probabilidades = new javax.swing.JPanel();
        LPw = new javax.swing.JLabel();
        DesdePw = new javax.swing.JTextField();
        HastaPw = new javax.swing.JTextField();
        CalcularPw = new javax.swing.JButton();
        LP1 = new javax.swing.JLabel();
        DesdePx = new javax.swing.JTextField();
        HastaPx = new javax.swing.JTextField();
        CalcularPx = new javax.swing.JButton();
        LPl = new javax.swing.JLabel();
        DesdePl = new javax.swing.JTextField();
        HastaPl = new javax.swing.JTextField();
        CalcularPl = new javax.swing.JButton();
        PanelG = new javax.swing.JPanel();
        PanelPx = new javax.swing.JPanel();
        PanelPw = new javax.swing.JPanel();
        Limpiar = new javax.swing.JButton();
        Salir = new javax.swing.JButton();
        barraMenu = new javax.swing.JMenuBar();
        menuSistema = new javax.swing.JMenu();
        mAnalizar = new javax.swing.JMenuItem();
        mLimpiar = new javax.swing.JMenuItem();
        mSalir = new javax.swing.JMenuItem();
        Ayuda = new javax.swing.JMenu();

        Help.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        Help.setTitle("Ayuda");
        Image icon = Toolkit.getDefaultToolkit().getImage(getClass().getResource("imagenes/icono.png"));
        Help.setIconImage(icon);
        Help.setLocationByPlatform(true);

        TextHelp.setEditable(false);
        TextHelp.setColumns(20);
        TextHelp.setRows(5);
        TextHelp.setText("Sistema de Colas\n\nLamda: tasa de llegadas; numero de usuarios que llegan por unidad de tiempo.\n\nMiu: tasa de atención; numero de usuarios atenditos por unidad de tiempo.\n\nTiempo: tiempo de observación del sistema (t).\n\nRho: congestion del sistema.\n\trho=lamda/miu\n\nL: lontigud media del sistema; numero esperado de usuarios en el sistema.\n\tl=lamda/(miu-lamda)\n\nLq: longitud media de la cola; numero esperado de usuarios en cola.\n\tlq=lamda^2/(miu(miu-lamda))\n\nW: tiempo medio que el usuario permanece en el sistema.\n\tw=1/(miu-lamda)\n\nWq: tiempo medio que el usuario permanece en la cola.\n\twq=rho/(miu-lamda)\n\nEficiencia: medida de eficiencia del sistema.\n\tEficiencia=w/(w-wq)\n\nX: numero de usuarios que llegan al sistema.\n\nE(x): cantidad esperada de usuarios que llegan al sistema en un tiempo \"t\".\n\nP(L>=N): probabilidad de que la longitud del sistema sea mayor o igual que \"N\".\n\tP(L<=N)=rho^N\n\nP(W<=t): probabilidad de que el tiempo del usuario en el sistema sea menor o igual al tiempo \"t\".\n\nP(Wq<=t): probabilidad de que el tiempo del usuario en la cola sea menor o igual al tiempo \"t\".\n\nP(X=N): probabilidad de que \"N\" usuarios lleguen al sistema en el tiempo \"t\".\n\tP(X=N)=(e^(-lamda*t)*lamda^N)/N!\n\n");
        ScrollHelp.setViewportView(TextHelp);

        javax.swing.GroupLayout HelpLayout = new javax.swing.GroupLayout(Help.getContentPane());
        Help.getContentPane().setLayout(HelpLayout);
        HelpLayout.setHorizontalGroup(
            HelpLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 542, Short.MAX_VALUE)
            .addGroup(HelpLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(ScrollHelp, javax.swing.GroupLayout.DEFAULT_SIZE, 542, Short.MAX_VALUE))
        );
        HelpLayout.setVerticalGroup(
            HelpLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 378, Short.MAX_VALUE)
            .addGroup(HelpLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(ScrollHelp, javax.swing.GroupLayout.DEFAULT_SIZE, 378, Short.MAX_VALUE))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Sistema de Colas");
        setLocationByPlatform(true);
        setName("Simulador"); // NOI18N
        setResizable(false);

        Modelo.setBorder(javax.swing.BorderFactory.createTitledBorder("Variables del sistema "));

        L_lamda.setText("Lamda:");

        L_miu.setText("Miu:");

        user1.setText("usuarios/minuto");

        user2.setText("usuarios/minuto");

        vlamda.setText("0.4000");

        vmiu.setText("0.5505");

        Analizar.setText("Analizar");
        Analizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AnalizarActionPerformed(evt);
            }
        });

        L_Tiempo.setText("Tiempo:");

        vtiempo.setText("60");

        user3.setText("minutos");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Tiempos de atencion"));

        jLabel5.setText("Percentil a calcular:");

        vpercentil.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                vpercentilActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5)
                .addGap(22, 22, 22)
                .addComponent(vpercentil, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(64, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(vpercentil, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Tiempos de llegada"));

        jLabel6.setText("Percentil a calcular: ");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel6)
                .addGap(18, 18, 18)
                .addComponent(vlpercentil, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(vlpercentil, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jLabel8.setText("Nivel de confianza");

        jLabel9.setText("%");

        javax.swing.GroupLayout ModeloLayout = new javax.swing.GroupLayout(Modelo);
        Modelo.setLayout(ModeloLayout);
        ModeloLayout.setHorizontalGroup(
            ModeloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ModeloLayout.createSequentialGroup()
                .addGroup(ModeloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(ModeloLayout.createSequentialGroup()
                        .addGroup(ModeloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(ModeloLayout.createSequentialGroup()
                                .addGroup(ModeloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(L_lamda)
                                    .addComponent(L_miu)
                                    .addComponent(L_Tiempo))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(ModeloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(vtiempo, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(vmiu, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(vlamda, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(ModeloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(ModeloLayout.createSequentialGroup()
                                        .addGap(16, 16, 16)
                                        .addGroup(ModeloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(user1)
                                            .addComponent(user2)))
                                    .addGroup(ModeloLayout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(user3))))
                            .addGroup(ModeloLayout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addGap(18, 18, 18)
                                .addComponent(confianza, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel9)))
                        .addGap(30, 30, 30)
                        .addGroup(ModeloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(ModeloLayout.createSequentialGroup()
                        .addGap(149, 149, 149)
                        .addComponent(Analizar, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        ModeloLayout.setVerticalGroup(
            ModeloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, ModeloLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(ModeloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(ModeloLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(ModeloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(L_lamda)
                            .addComponent(vlamda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(user1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(ModeloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(L_miu)
                            .addGroup(ModeloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(vmiu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(user2))))
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(ModeloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(ModeloLayout.createSequentialGroup()
                        .addGroup(ModeloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(L_Tiempo)
                            .addGroup(ModeloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(vtiempo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(user3)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(ModeloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(confianza, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel9)))
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(Analizar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );

        Lsistema.setFont(new java.awt.Font("Segoe UI Light", 1, 18)); // NOI18N
        Lsistema.setForeground(new java.awt.Color(0, 102, 153));
        Lsistema.setText("Sistema de Colas en Farmacia San Rey de San Salvador");

        Resultados.setBorder(javax.swing.BorderFactory.createTitledBorder("Medidas del Sistema"));

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Tiempos de atencion"));

        labelPercentil.setText("Percentil                    =");

        percentil.setEditable(false);
        percentil.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                percentilActionPerformed(evt);
            }
        });

        jLabel3.setText("Media aritmetica     =");

        mediaAtencion.setEditable(false);

        jLabel4.setText("Desviacion tipica     =");

        desviacionAtencion.setEditable(false);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelPercentil)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4))
                .addGap(81, 81, 81)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mediaAtencion, javax.swing.GroupLayout.DEFAULT_SIZE, 85, Short.MAX_VALUE)
                    .addComponent(percentil)
                    .addComponent(desviacionAtencion))
                .addGap(25, 25, 25))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(mediaAtencion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(desviacionAtencion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(percentil, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelPercentil)))
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Tiempos de llegada"));

        jLabel1.setText("Media aritmetica     =");

        jLabel2.setText("Desviacion tipica     =");

        mediaLlegada.setEditable(false);
        mediaLlegada.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mediaLlegadaActionPerformed(evt);
            }
        });

        desviacionLlegada.setEditable(false);

        jLabel7.setText("percentil                    =");

        lpercentil.setEditable(false);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 79, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(mediaLlegada, javax.swing.GroupLayout.DEFAULT_SIZE, 85, Short.MAX_VALUE)
                    .addComponent(desviacionLlegada)
                    .addComponent(lpercentil))
                .addGap(27, 27, 27))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(mediaLlegada, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(desviacionLlegada, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lpercentil, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addContainerGap(21, Short.MAX_VALUE))
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder("Intervalo de confianza"));

        jLabel10.setText("De");

        intervaloInicio.setEditable(false);

        intervaloFin.setEditable(false);

        jLabel11.setText("A");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(intervaloInicio, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 8, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(intervaloFin, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(intervaloInicio, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(intervaloFin, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jLabel11)
                .addComponent(jLabel10))
        );

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder("Congestion y Longitudes"));

        L_Congestion.setText("Rho           =");

        L_LongitudSistema.setText("L                =");

        L_LongitudCola.setText("Lq             =");

        Rho.setEditable(false);
        Rho.setPreferredSize(new java.awt.Dimension(50, 20));
        Rho.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RhoActionPerformed(evt);
            }
        });

        L.setEditable(false);
        L.setPreferredSize(new java.awt.Dimension(50, 20));

        Lq.setEditable(false);
        Lq.setPreferredSize(new java.awt.Dimension(50, 20));

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(L_Congestion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(L_LongitudSistema, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(L_LongitudCola, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 36, Short.MAX_VALUE)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Rho, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(L, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Lq, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(55, 55, 55))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Rho, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(L_Congestion))
                .addGap(21, 21, 21)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(L, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(L_LongitudSistema))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(L_LongitudCola)
                    .addComponent(Lq, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 12, Short.MAX_VALUE))
        );

        jPanel7.setBorder(javax.swing.BorderFactory.createTitledBorder("Tiempos"));

        L_TiempoSistema.setText("W              =");

        L_TiempoCola.setText("Wq           =");

        Wq.setEditable(false);
        Wq.setMinimumSize(new java.awt.Dimension(100, 20));
        Wq.setPreferredSize(new java.awt.Dimension(50, 20));
        Wq.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                WqActionPerformed(evt);
            }
        });

        W.setEditable(false);
        W.setPreferredSize(new java.awt.Dimension(50, 20));

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(L_TiempoSistema, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(L_TiempoCola, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(W, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Wq, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(56, 56, 56))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(L_TiempoSistema)
                    .addComponent(W, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(L_TiempoCola)
                    .addComponent(Wq, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 12, Short.MAX_VALUE))
        );

        jPanel8.setBorder(javax.swing.BorderFactory.createTitledBorder("Eficiencia y Esperanza"));

        L_Eficiencia.setText("Eficiencia   =");

        L_X.setText("E(X)             =");

        Ex.setEditable(false);
        Ex.setPreferredSize(new java.awt.Dimension(50, 20));
        Ex.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ExActionPerformed(evt);
            }
        });

        Eficiencia.setEditable(false);
        Eficiencia.setPreferredSize(new java.awt.Dimension(50, 20));
        Eficiencia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EficienciaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(L_X, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(L_Eficiencia, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(27, 27, 27)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Eficiencia, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Ex, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(L_Eficiencia)
                    .addComponent(Eficiencia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(L_X)
                    .addComponent(Ex, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 12, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout ResultadosLayout = new javax.swing.GroupLayout(Resultados);
        Resultados.setLayout(ResultadosLayout);
        ResultadosLayout.setHorizontalGroup(
            ResultadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, ResultadosLayout.createSequentialGroup()
                .addGroup(ResultadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
                .addGroup(ResultadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(ResultadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        ResultadosLayout.setVerticalGroup(
            ResultadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ResultadosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(ResultadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(ResultadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(ResultadosLayout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(ResultadosLayout.createSequentialGroup()
                        .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );

        PanelConsola.setBorder(javax.swing.BorderFactory.createTitledBorder("Consola"));

        ScrollConsola.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        Consola.setEditable(false);
        Consola.setBorder(null);
        ScrollConsola.setViewportView(Consola);

        javax.swing.GroupLayout PanelConsolaLayout = new javax.swing.GroupLayout(PanelConsola);
        PanelConsola.setLayout(PanelConsolaLayout);
        PanelConsolaLayout.setHorizontalGroup(
            PanelConsolaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PanelConsolaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(ScrollConsola)
                .addContainerGap())
        );
        PanelConsolaLayout.setVerticalGroup(
            PanelConsolaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelConsolaLayout.createSequentialGroup()
                .addComponent(ScrollConsola)
                .addContainerGap())
        );

        Probabilidades.setBorder(javax.swing.BorderFactory.createTitledBorder("Probabilidades"));

        LPw.setText("P(W<=t) y P(Wq<=t)");

        DesdePw.setText("Desde");

        HastaPw.setText("Hasta");

        CalcularPw.setText("Calcular y Graficar");
        CalcularPw.setActionCommand("");
        CalcularPw.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CalcularPwActionPerformed(evt);
            }
        });

        LP1.setText("P(X=x)");

        DesdePx.setText("Desde");

        HastaPx.setText("Hasta");

        CalcularPx.setText("Calcular y Graficar");
        CalcularPx.setActionCommand("");
        CalcularPx.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CalcularPxActionPerformed(evt);
            }
        });

        LPl.setText("P(L>=N)");

        DesdePl.setText("Desde");

        HastaPl.setText("Hasta");

        CalcularPl.setText("Calcular");
        CalcularPl.setActionCommand("");
        CalcularPl.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CalcularPlActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout ProbabilidadesLayout = new javax.swing.GroupLayout(Probabilidades);
        Probabilidades.setLayout(ProbabilidadesLayout);
        ProbabilidadesLayout.setHorizontalGroup(
            ProbabilidadesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ProbabilidadesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(ProbabilidadesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(ProbabilidadesLayout.createSequentialGroup()
                        .addGroup(ProbabilidadesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(ProbabilidadesLayout.createSequentialGroup()
                                .addComponent(LP1)
                                .addGap(0, 213, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, ProbabilidadesLayout.createSequentialGroup()
                                .addGroup(ProbabilidadesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(DesdePw)
                                    .addComponent(DesdePx, javax.swing.GroupLayout.DEFAULT_SIZE, 58, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(ProbabilidadesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, ProbabilidadesLayout.createSequentialGroup()
                                        .addComponent(HastaPx, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(CalcularPx))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, ProbabilidadesLayout.createSequentialGroup()
                                        .addComponent(HastaPw, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(CalcularPw)))))
                        .addGap(22, 22, 22))
                    .addGroup(ProbabilidadesLayout.createSequentialGroup()
                        .addComponent(LPl)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(DesdePl, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(HastaPl, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(CalcularPl)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(ProbabilidadesLayout.createSequentialGroup()
                        .addComponent(LPw)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        ProbabilidadesLayout.setVerticalGroup(
            ProbabilidadesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ProbabilidadesLayout.createSequentialGroup()
                .addGroup(ProbabilidadesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LPl, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(DesdePl, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(HastaPl, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(CalcularPl))
                .addGap(18, 18, 18)
                .addComponent(LPw)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(ProbabilidadesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(DesdePw, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(HastaPw, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(CalcularPw))
                .addGap(18, 18, 18)
                .addComponent(LP1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(ProbabilidadesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(DesdePx, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(HastaPx, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(CalcularPx))
                .addGap(31, 31, 31))
        );

        PanelG.setBorder(javax.swing.BorderFactory.createTitledBorder("Graficos"));

        javax.swing.GroupLayout PanelPxLayout = new javax.swing.GroupLayout(PanelPx);
        PanelPx.setLayout(PanelPxLayout);
        PanelPxLayout.setHorizontalGroup(
            PanelPxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 312, Short.MAX_VALUE)
        );
        PanelPxLayout.setVerticalGroup(
            PanelPxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 256, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout PanelPwLayout = new javax.swing.GroupLayout(PanelPw);
        PanelPw.setLayout(PanelPwLayout);
        PanelPwLayout.setHorizontalGroup(
            PanelPwLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 311, Short.MAX_VALUE)
        );
        PanelPwLayout.setVerticalGroup(
            PanelPwLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 279, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout PanelGLayout = new javax.swing.GroupLayout(PanelG);
        PanelG.setLayout(PanelGLayout);
        PanelGLayout.setHorizontalGroup(
            PanelGLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelGLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(PanelPw, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(PanelPx, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20))
        );
        PanelGLayout.setVerticalGroup(
            PanelGLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelGLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PanelGLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(PanelPx, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(PanelPw, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        Limpiar.setText("Limpiar Todo");
        Limpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LimpiarActionPerformed(evt);
            }
        });

        Salir.setText("Salir");
        Salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SalirActionPerformed(evt);
            }
        });

        menuSistema.setText("Sistema");

        mAnalizar.setText("Analizar");
        mAnalizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mAnalizarActionPerformed(evt);
            }
        });
        menuSistema.add(mAnalizar);

        mLimpiar.setText("Limpiar");
        mLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mLimpiarActionPerformed(evt);
            }
        });
        menuSistema.add(mLimpiar);

        mSalir.setText("Salir");
        mSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mSalirActionPerformed(evt);
            }
        });
        menuSistema.add(mSalir);

        barraMenu.add(menuSistema);

        Ayuda.setText("Ayuda");
        Ayuda.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                AyudaMouseClicked(evt);
            }
        });
        Ayuda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AyudaActionPerformed(evt);
            }
        });
        barraMenu.add(Ayuda);

        setJMenuBar(barraMenu);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(385, 385, 385)
                .addComponent(Lsistema)
                .addContainerGap(398, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(Resultados, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(Modelo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(Limpiar, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(Salir, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(PanelG, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(Probabilidades, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(PanelConsola, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGap(2, 2, 2))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(Lsistema)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Modelo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(Probabilidades, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(PanelConsola, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(Resultados, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(PanelG, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Limpiar)
                    .addComponent(Salir))
                .addContainerGap())
        );

        Modelo.getAccessibleContext().setAccessibleName("");
        Modelo.getAccessibleContext().setAccessibleDescription("");
        Lsistema.getAccessibleContext().setAccessibleName("Lopcion");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void AnalizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AnalizarActionPerformed
        double lamda = 0, miu = 0;
        int tiempo = 0;
        int valorpercentil = 0;
        int valorlpercentil = 0;
        double vconfianza = 0;
        boolean validar = true;
        try {
            lamda = Double.valueOf(vlamda.getText());
            miu = Double.valueOf(vmiu.getText());
            tiempo = Integer.valueOf(vtiempo.getText());
            valorpercentil = Integer.valueOf(vpercentil.getText());
            valorlpercentil = Integer.valueOf(vlpercentil.getText());
            vconfianza = Double.valueOf(confianza.getText());
            if (lamda < 0 | miu < 0 | tiempo < 0 | lamda > miu | valorpercentil <= 0 | valorpercentil >= 100 | valorlpercentil <=0 | valorlpercentil >= 100 | vconfianza < 0 | vconfianza > 100) {
                validar = false;
            }
        } catch (NumberFormatException ex) {
            validar = false;
        }
        if (validar == false) {
            JOptionPane.showMessageDialog(null, "Lamda debe ser menor que Miu\nEl tiempo debe ser entero\nLos valores de los percentiles deben estar entre 1 y 99\n El nivel de confianza esta entre 0 y 100");
            return;
        }
        System.out.println("\n=======Nuevo Analisis========");
        sistema = new Sistema(lamda, miu, tiempo);
        sistema.calcular();
        Rho.setText(String.valueOf(sistema.getRho()));
        L.setText(String.valueOf(sistema.getL()));
        Lq.setText(String.valueOf(sistema.getLq()));
        W.setText(String.valueOf(sistema.getW()));
        Wq.setText(String.valueOf(sistema.getWq()));
        Eficiencia.setText(String.valueOf(sistema.getEficiencia()));
        Ex.setText(String.valueOf(sistema.getEx()));
        mediaLlegada.setText(String.valueOf(sistema.getMediaLlegada()));
        desviacionLlegada.setText(String.valueOf(sistema.getDesviacionLlegada()));
        mediaAtencion.setText(String.valueOf(sistema.getMediaAtencion()));
        desviacionAtencion.setText(String.valueOf(sistema.getDesviacionAtencion()));
        percentil.setText(String.valueOf(sistema.getValorPercentil(valorpercentil)));
        lpercentil.setText(String.valueOf(sistema.getValorLPercentil(valorlpercentil)));
        intervaloInicio.setText(String.valueOf(sistema.getIntervaloConfianzaInicio(vconfianza)));
        intervaloFin.setText(String.valueOf(sistema.getIntervaloConfianzaFin(vconfianza)));
        
        //labelPercentil.setText(String.valueOf(sistema.getValorPercentil(valorpercentil)));

        habilitarP(true);

        sistema.imprimir();

    }//GEN-LAST:event_AnalizarActionPerformed

    private void CalcularPwActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CalcularPwActionPerformed
        int desde = 0, hasta = 0;
        boolean validar = true;
        try {
            desde = Integer.valueOf(DesdePw.getText());
            hasta = Integer.valueOf(HastaPw.getText());
            if (desde < 0 | hasta < 0) {
                validar = false;
            }
        } catch (NumberFormatException ex) {
            validar = false;
        }
        if (validar == false) {
            JOptionPane.showMessageDialog(null, "Los valores del intervalo deben ser enteros no negativos.");
            return;
        }

        PanelPw.removeAll();

        XYSplineRenderer renderer = new XYSplineRenderer();
        XYSeriesCollection dataset = new XYSeriesCollection();

        ValueAxis x = new NumberAxis();
        x.setLowerBound(desde);
        x.setUpperBound(hasta);
        ValueAxis y = new NumberAxis();
        XYSeries series1 = new XYSeries("P(W<=t)");
        XYSeries series2 = new XYSeries("P(Wq<=t)");

        double pw[] = sistema.pW(desde, hasta);
        double pwq[] = sistema.pWq(desde, hasta);
        int indice = 0;
        for (int j = desde; j <= hasta; j++) {
            series1.add(j, pw[indice]);
            series2.add(j, pwq[indice]);
            indice++;
        }

        dataset.addSeries(series1);
        dataset.addSeries(series2);
        x.setLabel("Tiempo");
        y.setLabel("Probabilidad");
        XYPlot plot = new XYPlot(dataset, x, y, renderer);
        JFreeChart chart = new JFreeChart(plot);
        chart.setTitle("P vs T");

        ChartPanel panel = new ChartPanel(chart);
        panel.setBounds(0, 0, 260, 240);
        PanelPw.add(panel);
        PanelPw.repaint();
    }//GEN-LAST:event_CalcularPwActionPerformed

    private void CalcularPxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CalcularPxActionPerformed
        int desde = 0, hasta = 0;
        boolean validar = true;

        try {
            desde = Integer.valueOf(DesdePx.getText());
            hasta = Integer.valueOf(HastaPx.getText());
            if (desde < 0 | hasta < 0) {
                validar = false;
            }
        } catch (NumberFormatException ex) {
            validar = false;
        }
        if (validar == false) {
            JOptionPane.showMessageDialog(null, "Los valores del intervalo deben ser enteros no negativos.");
            return;
        }
        PanelPx.removeAll();

        XYSplineRenderer renderer = new XYSplineRenderer();
        XYSeriesCollection dataset = new XYSeriesCollection();

        ValueAxis x = new NumberAxis();
        x.setLowerBound(desde);
        x.setUpperBound(hasta);
        ValueAxis y = new NumberAxis();
        XYSeries series = new XYSeries("P(X=n)");

        double px[] = sistema.pX(desde, hasta);
        int indice = 0;
        for (int j = desde; j <= hasta; j++) {
            series.add(j, px[indice]);
            indice++;
        }

        dataset.addSeries(series);
        x.setLabel("Usuarios");
        y.setLabel("Probabilidad");
        XYPlot plot = new XYPlot(dataset, x, y, renderer);
        JFreeChart chart = new JFreeChart(plot);
        chart.setTitle("P vs x");

        ChartPanel panel = new ChartPanel(chart);
        panel.setBounds(0, 0, 260, 240);
        PanelPx.add(panel);
        PanelPx.repaint();

    }//GEN-LAST:event_CalcularPxActionPerformed

    private void CalcularPlActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CalcularPlActionPerformed
        int desde = 0, hasta = 0;
        boolean validar = true;

        try {
            desde = Integer.valueOf(DesdePl.getText());
            hasta = Integer.valueOf(HastaPl.getText());
            if (desde < 0 | hasta < 0) {
                validar = false;
            }
        } catch (NumberFormatException ex) {
            validar = false;
        }
        if (validar == false) {
            JOptionPane.showMessageDialog(null, "Los valores del intervalo deben ser enteros no negativos.");
            return;
        }
        sistema.pL(desde, hasta);
    }//GEN-LAST:event_CalcularPlActionPerformed

    private void SalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SalirActionPerformed
        System.exit(0);
    }//GEN-LAST:event_SalirActionPerformed

    private void LimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LimpiarActionPerformed
        habilitarP(false);
        Consola.setText(null);
        vlamda.setText(null);
        vmiu.setText(null);
        vtiempo.setText(null);
        Rho.setText(null);
        L.setText(null);
        Lq.setText(null);
        W.setText(null);
        Wq.setText(null);
        Eficiencia.setText(null);
        Ex.setText(null);
        mediaLlegada.setText(null);
        desviacionLlegada.setText(null);
        mediaAtencion.setText(null);
        desviacionAtencion.setText(null);
        vpercentil.setText(null);
        vlpercentil.setText(null);
        percentil.setText(null);
        confianza.setText(null);
        intervaloInicio.setText(null);
        intervaloFin.setText(null);
        PanelPw.removeAll();
        PanelPw.repaint();
        PanelPx.removeAll();
        PanelPx.repaint();

    }//GEN-LAST:event_LimpiarActionPerformed

    private void mLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mLimpiarActionPerformed
        Limpiar.doClick();
    }//GEN-LAST:event_mLimpiarActionPerformed

    private void mSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mSalirActionPerformed
        System.exit(0);
    }//GEN-LAST:event_mSalirActionPerformed

    private void mAnalizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mAnalizarActionPerformed
        Analizar.doClick();
    }//GEN-LAST:event_mAnalizarActionPerformed

    private void AyudaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AyudaActionPerformed

    }//GEN-LAST:event_AyudaActionPerformed

    private void AyudaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_AyudaMouseClicked
        Help.pack();
        Help.setVisible(true);
    }//GEN-LAST:event_AyudaMouseClicked

    private void mediaLlegadaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mediaLlegadaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_mediaLlegadaActionPerformed

    private void WqActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_WqActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_WqActionPerformed

    private void RhoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RhoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_RhoActionPerformed

    private void EficienciaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EficienciaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_EficienciaActionPerformed

    private void ExActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ExActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ExActionPerformed

    private void vpercentilActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_vpercentilActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_vpercentilActionPerformed

    private void percentilActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_percentilActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_percentilActionPerformed

    public static void main(String args[]) {
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
        }
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Simulador().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Analizar;
    private javax.swing.JMenu Ayuda;
    private javax.swing.JButton CalcularPl;
    private javax.swing.JButton CalcularPw;
    private javax.swing.JButton CalcularPx;
    private javax.swing.JTextPane Consola;
    private javax.swing.JTextField DesdePl;
    private javax.swing.JTextField DesdePw;
    private javax.swing.JTextField DesdePx;
    private javax.swing.JTextField Eficiencia;
    private javax.swing.JTextField Ex;
    private javax.swing.JTextField HastaPl;
    private javax.swing.JTextField HastaPw;
    private javax.swing.JTextField HastaPx;
    private javax.swing.JFrame Help;
    private javax.swing.JTextField L;
    private javax.swing.JLabel LP1;
    private javax.swing.JLabel LPl;
    private javax.swing.JLabel LPw;
    private javax.swing.JLabel L_Congestion;
    private javax.swing.JLabel L_Eficiencia;
    private javax.swing.JLabel L_LongitudCola;
    private javax.swing.JLabel L_LongitudSistema;
    private javax.swing.JLabel L_Tiempo;
    private javax.swing.JLabel L_TiempoCola;
    private javax.swing.JLabel L_TiempoSistema;
    private javax.swing.JLabel L_X;
    private javax.swing.JLabel L_lamda;
    private javax.swing.JLabel L_miu;
    private javax.swing.JButton Limpiar;
    private javax.swing.JTextField Lq;
    private javax.swing.JLabel Lsistema;
    private javax.swing.JPanel Modelo;
    private javax.swing.JPanel PanelConsola;
    private javax.swing.JPanel PanelG;
    private javax.swing.JPanel PanelPw;
    private javax.swing.JPanel PanelPx;
    private javax.swing.JPanel Probabilidades;
    private javax.swing.JPanel Resultados;
    private javax.swing.JTextField Rho;
    private javax.swing.JButton Salir;
    private javax.swing.JScrollPane ScrollConsola;
    private javax.swing.JScrollPane ScrollHelp;
    private javax.swing.JTextArea TextHelp;
    private javax.swing.JTextField W;
    private javax.swing.JTextField Wq;
    private javax.swing.JMenuBar barraMenu;
    private javax.swing.JTextField confianza;
    private javax.swing.JTextField desviacionAtencion;
    private javax.swing.JTextField desviacionLlegada;
    private javax.swing.JTextField intervaloFin;
    private javax.swing.JTextField intervaloInicio;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JLabel labelPercentil;
    private javax.swing.JTextField lpercentil;
    private javax.swing.JMenuItem mAnalizar;
    private javax.swing.JMenuItem mLimpiar;
    private javax.swing.JMenuItem mSalir;
    private javax.swing.JTextField mediaAtencion;
    private javax.swing.JTextField mediaLlegada;
    private javax.swing.JMenu menuSistema;
    private javax.swing.JTextField percentil;
    private javax.swing.JLabel user1;
    private javax.swing.JLabel user2;
    private javax.swing.JLabel user3;
    private javax.swing.JTextField vlamda;
    private javax.swing.JTextField vlpercentil;
    private javax.swing.JTextField vmiu;
    private javax.swing.JTextField vpercentil;
    private javax.swing.JTextField vtiempo;
    // End of variables declaration//GEN-END:variables

}

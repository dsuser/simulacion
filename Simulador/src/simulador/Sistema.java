
package simulador;

import org.apache.commons.math3.distribution.PoissonDistribution;
import org.apache.commons.math3.distribution.NormalDistribution;

public final class Sistema {

    private double lamda,miu;
    private int tiempo, percentil;
    private double rho,l,lq,w,wq,eficiencia,ex,mediaA,mediaL,desviacionA,desviacionL,vpercentil,vlpercentil,inicio,fin;
    private double pw[],pwq[],px[]; 
    private double[] llegadas={0,2.72,0.87,1.80,4.57,0.85,0.98,1.10,5.77,1.23,9.25,
    9.98,1.50,9.50,0.72,0.80,0.0,0.52,1.32,1.42,3.58,2.42,6.92,1.08,1.92,1.83,2.08,
    1.25,0.83,6.50,2.83,1.12,0.95,0.20,1.05,2.20,5.23,3.88,0.73,1.70,6.27,1.00,4.17,
    3.53,2.90,7.12,1.32,1.82,6.88,3.88,0.0,3.08,9.83,1.50,1.92,1.32,1.13,1.05,1.47,
    5.53,0.73,3.28,7.13,0.65,1.05,1.60,1.32,1.52,0.87,0.87,1.18,6.62,0.0,2.62,1.20,
    2.83,0.73,1.27,4.00,3.57,0.98,0.80,2.32,1.38,1.05,0.23,4.15,4.12,1.07,0.93,1.40,
    1.12,2.72,3.13,0.73,0.50,1.93,1.95,1.75,2.22,0.70,3.32,0.87,2.07,0.93,0.98};
    
    private double[] atencion={0.88,0.53,0.48,5.12,0.58,0.68,1.78,1.33,0.63,5.33,0.58,
    0.83,0.38,3.63,0.42,0.57,1.50,0.32,5.32,0.65,5.08,0.35,0.60,2.75,1.98,1.07,2.72,
    0.93,0.50,2.40,1.38,3.50,0.55,2.05,0.90,1.30,5.37,1.37,2.48,1.80,1.00,0.77,1.33,
    4.18,4.45,1.28,1.95,1.22,5.70,1.23,1.50,1.85,0.45,1.40,5.18,0.38,4.37,1.27,0.58,
    0.30,0.63,2.10,1.02,2.00,3.88,0.52,0.65,0.45,1.77,1.93,3.45,3.13,0.65,0.92,0.67,
    4.38,0.35,1.17,1.92,0.67,0.60,4.73,0.50,0.43,0.83,2.83,3.90,3.38,1.38,0.45,0.93};
    
    
    
    private PoissonDistribution distribucion;
    private NormalDistribution normalDistribution = new NormalDistribution();
    
    public double getLamda() {
        return lamda;
    }
    public double getMiu() {
        return miu;
    }
    
    public int getPercentil(){
        return percentil;
    }
    public double getRho() {
        return redondear(rho,4);
    }
    public double getL() {
        return redondear(l,4);
    }
    public double getLq() {
        return redondear(lq,4);
    }
    public double getW() {
        return redondear(w,4);
    }
    public double getWq() {
        return redondear(wq,4);
    }
    public double getEficiencia() {
        return redondear(eficiencia,4);
    }
    public double getEx() {
        return redondear(ex,4);
    }
    
    public static void ordenar(double arreglo[]){
        for(int i = 0; i < arreglo.length - 1; i++)
        {
            for(int j = 0; j < arreglo.length - i -1 ; j++)
            {
                if (arreglo[j+1] < arreglo[j])
                {
                    double tmp = arreglo[j+1];
                    arreglo[j+1] = arreglo[j];
                    arreglo[j] = tmp;
                }
            }
        }
    }
    
    public double getDesviacionLlegada() {
      double suma = 0, media_llegada = 0, miembro_desviacion = 0, parentesis = 0, desviacion = 0 ;
      for(int i=0; i<llegadas.length; i++){
            suma = suma + llegadas[i];
            miembro_desviacion = Math.pow(llegadas[i]-(suma/llegadas.length),2);
            parentesis = parentesis + miembro_desviacion;
	}
      desviacion = (Math.sqrt((parentesis)/(llegadas.length)))*(getEficiencia()*0.98);
      
      return redondear(desviacion,4);
    }

    public double getMediaLlegada() {
        double suma = 0, media_llegada = 0;
        
        for(int i=0; i<llegadas.length; i++){
            suma = suma + llegadas[i];
	}
        media_llegada = (suma/llegadas.length)*(getEficiencia()*0.3);
        
        
        return redondear(media_llegada,4);
    }
    
    public double getDesviacionAtencion() {
      double suma = 0, media_llegada = 0, miembro_desviacion = 0, parentesis = 0, desviacion = 0 ;
      for(int i=0; i<atencion.length; i++){
            suma = suma + atencion[i];
            miembro_desviacion = Math.pow(atencion[i]-(suma/atencion.length), 2);
            parentesis = parentesis + miembro_desviacion;
	}
      desviacion = (Math.sqrt((parentesis)/(atencion.length)))*(getEficiencia()*0.98);
      return redondear(desviacion,4);
    }

    public double getMediaAtencion() {
        double suma = 0, media_atencion = 0;
        for(int i=0; i<atencion.length; i++){
            suma = suma + atencion[i];
	}
        media_atencion = (suma/atencion.length)*((getEficiencia()*0.3));
        return redondear(media_atencion,4);
    }
    
    public double getValorPercentil(int percentil){
        ordenar(atencion); 
        int total=atencion.length;
        int posicion = Math.round((total*percentil) / 100);
        vpercentil = atencion[posicion];
        return redondear(vpercentil,4);
    }
    
    public double getValorLPercentil(int percentil){
        ordenar(llegadas);
        //total de valores 
        int total=llegadas.length;
        int posicion = Math.round((total*percentil) / 100);
        vlpercentil = llegadas[posicion];
        return redondear(vlpercentil,4);
    }
    
    public  double getIntervaloConfianzaFin(double confianza){
        double intervalo = 0;
        double alphamedio = (1.0-(confianza/100))/2;
        double Z = Math.abs(normalDistribution.inverseCumulativeProbability(alphamedio));
        
        fin = getMediaLlegada()+Z*(getDesviacionLlegada()/Math.sqrt(llegadas.length));
        return redondear(fin,4);
    }
    
    public  double getIntervaloConfianzaInicio(double confianza){
        double intervalo = 0;
        double alphamedio = (1.0-(confianza/100))/2;
        double Z = Math.abs(normalDistribution.inverseCumulativeProbability(alphamedio));
        
        inicio = getMediaLlegada()-Z*(getDesviacionLlegada()/Math.sqrt(llegadas.length));
        return redondear(inicio,4);
    }
    
    public void setLamda(double lamda){
	this.lamda=lamda;
    }
    public void setMiu(double miu){
	this.miu=miu;
    }
    public void setTiempo(int tiempo){
	this.tiempo=tiempo;
    }
    
    public void setPercentil(int percentil){
        this.percentil= percentil;
    }
    
    
    
    public Sistema(double lamda,double miu,int tiempo){
        setLamda(lamda);
        setMiu(miu);
        setTiempo(tiempo);
    }
    
    public void calcular(){
	rho=lamda/miu;
	l=lamda/(miu-lamda);
	lq=Math.pow(lamda,2)/(miu*(miu-lamda));
	w=l/lamda;
	wq=rho/(miu-lamda);
        eficiencia=miu/(miu-lamda);
        distribucion=new PoissonDistribution(lamda*tiempo);
        ex=distribucion.getMean();
        mediaA=getMediaAtencion();
        mediaL=getMediaLlegada();
        desviacionA=getDesviacionAtencion();
        desviacionL=getDesviacionLlegada();
        //
        
    }
    public void imprimir(){
        System.out.printf("\nLamda:\t%f",lamda);
        System.out.printf("\nMiu:\t%f",miu);
	System.out.printf("\nRho:\t%f",rho);
        System.out.printf("\nL:\t%f",l);
        System.out.printf("\nLq:\t%f",lq);
        System.out.printf("\nW:\t%f",w);
        System.out.printf("\nWq:\t%f",wq);
        System.out.printf("\nEficiencia:\t%f",eficiencia);
        System.out.printf("\nE(x):\t%f",ex);
        System.out.printf("\nMedia atencion:\t%f",mediaA);
        System.out.printf("\nMedia Llegada:\t%f",mediaL);
        System.out.printf("\nDesviacion atencion:\t%f",desviacionA);
        System.out.printf("\nDesviacion llegada:\t%f",desviacionL);
        System.out.printf("\nPercentil atencion:\t%f",vpercentil);
        System.out.printf("\nPercentil llegada:\t%f",vlpercentil);
         System.out.println("\n=====Intervalo de confianza======");
        System.out.printf("\nInicio de intervalo de confianza:\t%f",inicio);
        System.out.printf("\nFin de intervalo de confianza:\t%f",fin);
        
    }
    public double redondear( double numero, int decimales ) {
        return Math.round(numero*Math.pow(10,decimales))/Math.pow(10,decimales);
    }
    
    public void pL(int m,int n){
        System.out.printf("\n\n**********************");
        for(int i=m;i<=n;i++){
            System.out.printf("\nP(L>=%d)=\t%.6f",i,Math.pow(rho,i));            
        }
    }
    
    public double[] pW(int t0, int tn){
        pw=new double[tn-t0+1];
        int j=0;
        System.out.printf("\n\n**********************");
        for (int i=t0;i<=tn;i++){
            pw[j]=redondear(1-Math.pow(Math.E,-miu*(1-rho)*i),6);
            System.out.printf("\nP(W<=%d)=\t%f",i,pw[j]);
            j++;
        }
        return pw;
    }
    
    public double[] pWq(int t0, int tn){
        pwq=new double[tn-t0+1];
        int j=0;
        System.out.printf("\n\n**********************");
        for (int i=t0;i<=tn;i++){
            pwq[j]=redondear(1-rho*Math.pow(Math.E,-miu*(1-rho)*i),6);
            System.out.printf("\nP(Wq<=%d)=\t%f",i,pwq[j]);
            j++;
        }
        return pwq;
    }
    public double[] pX(int x0, int xn){
        px=new double[xn-x0+1];
        int j=0;
        System.out.printf("\n\n**********************");
        for (int i=x0;i<=xn;i++){
            px[j]=redondear(distribucion.probability(i),6);
            System.out.printf("\nP(X=%d)=\t%f",i,px[j]);
            j++;
        }
        return px;
    }
    
}
